
## dark-crystal-key-backup-rust

Provides encryption of secret shares to specific recipients using [`crypto_box`](https://docs.rs/crate/crypto_box)

Internally uses [`dark-crystal-secret-sharing-rust`](https://gitlab.com/dark-crystal-rust/dark-crystal-secret-sharing-rust) which uses [`sharks`](https://docs.rs/sharks/0.5.0/sharks/) for Shamirs secret sharing and [`xsalsa20poly1305`](https://docs.rs/xsalsa20poly1305/0.8.0/xsalsa20poly1305/) for authenticated encryption.

This is part of a work-in-progress Rust implementation of the [Dark Crystal Key Backup Protocol](https://darkcrystal.pw/protocol-specification/).

- [API documentation](https://docs.rs/dark-crystal-key-backup-rust)
